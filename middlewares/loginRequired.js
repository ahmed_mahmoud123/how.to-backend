var writeError = require('../utils/response').writeResponse,
validationRules = require('../utils/validationRules');

module.exports = function loginRequired(req, res, next) {
  if(req.method == 'OPTIONS')
    return res.status(200).end();

  var authHeader = req.headers['authorization'];
  if (!authHeader) {
    return writeError(res,validationRules.INVALID_AUTHORIZATION, 401);
  }
  if (!req.user.api_key) {
    return writeError(res,validationRules.INVALID_APIKEY, 403);
  }
  next();
};
