var writeError = require('../utils/response').writeError,
Users = require('../services/authService'),
dbUtils = require('../utils/neo4j/dbUtils'),
validationRules = require('../utils/validationRules');

module.exports = function setAuthUser(req, res, next) {
  var authHeader = req.headers['authorization'];
  if (!authHeader) {
    req.user = {id: null};
    next();
  }
  else {
    var match = authHeader.match(/^Token (\S+)/);
    if (!match || !match[1]) {
      return writeError(res, validationRules.INVALID_TOKEN, 401);
    }
    var token = match[1];

    Users.me(dbUtils.getSession(req), token)
      .then(user => {
        console.log(user);
        req.user = user;
        next();
      })
      .catch(error=>{
        console.log(error);
        return writeError(res, error, 403);
      });
  }
};
