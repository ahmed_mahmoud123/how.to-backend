"use strict"
var uuid = require('node-uuid');
var Course = require('../db/models/course');

var create = function (session, school_id, course_data) {
    return session.run('MATCH (school:School {id: {school_id}}) CREATE (course:Course {id: {id}, name: {name}, desc: {desc}, avatar:{avatar},cover:{cover} }) CREATE (school)-[r:contain]->(course) RETURN course',
      {
        id: uuid.v4(),
        school_id,
        name:course_data.name,
        desc:course_data.desc,
        avatar:course_data.avatar,
        cover:course_data.cover
      }
    ).then(results => {
        return new Course(results.records[0].get('course'));
      });
};

var createLevel = function (session, course_id, level_data) {
    return session.run('MATCH (course:Course {id: {course_id}}) CREATE (level:Level {id: {id}, name: {name}, duration: {duration}}) CREATE (course)-[r:has]->(level) RETURN level',
      {
        id: uuid.v4(),
        course_id,
        name:level_data.name,
        duration:level_data.duration,
      }
    ).then(results => {
        return new Course(results.records[0].get('course'));
      });
};

var getById = function (session, course_id) {
  return session.run('MATCH (course:Course {id: {course_id} } ) RETURN course',
    {
      course_id
    }).then(results => {
      return new Course(results.records[0].get('course'));
    });
};





module.exports = {create,createLevel,getById};
