"use strict"

var uuid = require('node-uuid');
var validationRules = require('../utils/validationRules');
var randomstring = require("randomstring");
var _ = require('lodash');
var User = require('../db/models/users');
var crypto = require('crypto');

var register = function (session, user) {
  return session.run('MATCH (user:User {email: {email}}) RETURN user', {email: user.email} )
    .then(results => {
      if (!_.isEmpty(results.records)) {
        throw validationRules.EMAIL_EXISTS;
      }
      else {
        return session.run('CREATE (user:User {id: {id},username:{username},last_name:{last_name},email:{email}, first_name: {first_name}, password: {password}, api_key: {api_key}}) RETURN user',
          {
            id: uuid.v4(),
            first_name: user.first_name,
            last_name: user.last_name,
            username:user.first_name+" "+user.last_name,
            email: user.email,
            password: hashPassword(user.email, user.password),
            api_key: randomstring.generate({
              length: 20,
              charset: 'hex'
            })
          }
        ).then(results => {
            return new User(results.records[0].get('user'));
        });
      }
    });
};

var me = function (session, apiKey) {
  return session.run('MATCH (user:User {api_key: {api_key}}) with user optional match (user)-[:interest]-(category:Category) RETURN user,category', {api_key: apiKey})
    .then(results => {
      if (_.isEmpty(results.records)) {
        throw validationRules.INVALID_APIKEY;
      }
      return new User(results.records[0].get('user'),results);
    });
};

var login = function (session, email, password) {
  return session.run('MATCH (user:User {email: {email}}) RETURN user', {email})
    .then(results => {
        if (_.isEmpty(results.records)) {
          throw validationRules.INVALID_USER;
        }
        else {
          var dbUser = _.get(results.records[0].get('user'), 'properties');
          if (dbUser.password != hashPassword(email, password)) {
            throw validationRules.WRONG_PASSWORD;
          }
          return {token: _.get(dbUser, 'api_key')};
        }
      }
    );
};

function hashPassword(email, password) {
  var s = email + ':' + password;
  return crypto.createHash('sha256').update(s).digest('hex');
}

module.exports = {
  register: register,
  me: me,
  login: login
};
