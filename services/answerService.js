"use strict"
var validationRules = require('../utils/validationRules');
var Answer = require('../db/models/answer');

var create = function (session, api_key, answer_data) {
    return session.run('MATCH (user:User {api_key: {api_key}}) Match (video:Video {id:{video_id} }) with user,video create (user)-[answer:Answer {text:{answer},createdAt:{createdAt} }]->(video) return answer,user',
      {
        api_key,
        answer:answer_data.answer,
        video_id:answer_data.video_id,
        createdAt: +new Date()
      }
    ).then(results => {
        return new Answer(results.records[0]);
      });
};

var list = function (session,video_id) {
  return session.run('MATCH (video:Video {id: {video_id}})<-[answer:Answer]-(user:User) with answer,user RETURN answer,user order by answer.createdAt DESC',
    {
      video_id
    }).then(results => {
      let answers = [] ;
      results.records.forEach(node=>{
        answers.push(new Answer(node));
      });
      return answers;
    });
};


module.exports = {create,list};
