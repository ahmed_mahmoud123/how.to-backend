"use strict"
var uuid = require('node-uuid');
var validationRules = require('../utils/validationRules');
var _ = require('lodash');
var Video = require('../db/models/video');
var Level = require('../db/models/level');
var VideoDetails = require('../db/models/videoDetails');

var publish = function (session, relation , video_data) {
  return session.run('MATCH (relation_node:'+relation.type+' {id: {relation_id}}) CREATE (video:Video {id: {id}, name: {name}, desc: {desc}, link: {link},duration:{duration},cover:{cover} }) CREATE (relation_node)-[r:has]->(video) RETURN video',
  {
    id: uuid.v4(),
    relation_id:relation.id,
    name:video_data.name,
    desc:video_data.desc,
    duration:video_data.duration,
    link:video_data.link,
    cover:video_data.cover
  }).then(results => {
    return new Video(results.records[0].get('video'));
  });
};




var list = function (session) {
  return session.run('match (video:Video) with video optional match (video)<-[:has]-(channel:Channel) optional match (video)<-[:has]-(level:Level) optional match (video)<-[:has*]-(course:Course) return course , level, channel,video')
  .then(results => {
    let videos = [];
    results.records.forEach(video=>{
      videos.push(new VideoDetails(video));
    });
    return videos;
  });
};

var getById = function (session,video_id) {
  return session.run('match (video:Video {id:{video_id} }) with video optional match (video)<-[:has]-(channel:Channel) optional match (video)<-[:has]-(level:Level) optional match (video)<-[:has*]-(course:Course) return  course , level, channel,video',
  {video_id})
  .then(results => {
    return new VideoDetails(results.records[0]);
  });
};

var like = async function(session,api_key,video_id){
  await removeDislike(session,api_key,video_id);
  return session.run('match (user:User {api_key:{api_key} }) match (video:Video {id:{video_id}}) create (user)-[liked:liked]->(video) return liked',
  {
    api_key,
    video_id
  }
  ).then(results => {
    return {result:1};
  });
}

var dislike = async function(session,api_key,video_id){
  await removeLike(session,api_key,video_id);
  return session.run('match (user:User {api_key:{api_key} }) match (video:Video {id:{video_id}}) create (user)-[disliked:disliked]->(video) return disliked',
  {
    api_key,
    video_id
  }
  ).then(results => {
    return {result:1};
  });
}

var removeLike = function(session,api_key,video_id){
  return session.run('match (user:User {api_key:{api_key} }) match (video:Video {id:{video_id}}) with user,video  match (user)-[liked:liked]->(video) detach delete liked return 1',
  {
    api_key,
    video_id
  }
  ).then(results => {
    return {result:1};
  });
}

var removeDislike = function(session,api_key,video_id){
  return session.run('match (user:User {api_key:{api_key} }) match (video:Video {id:{video_id}}) with user,video  match (user)-[disliked:disliked]->(video) detach delete disliked return 1',
  {
    api_key,
    video_id
  }
  ).then(results => {
    return {result:1};
  });
}



module.exports = {publish,list,getById,like,dislike};
