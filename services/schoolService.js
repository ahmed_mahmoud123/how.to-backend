"use strict"
var uuid = require('node-uuid');
var validationRules = require('../utils/validationRules');
var _ = require('lodash');
var School = require('../db/models/school');
var Course = require('../db/models/course');

var create = function (session, api_key, school_data) {
    return session.run('MATCH (user:User {api_key: {api_key}}) CREATE (school:School {id: {id}, name: {name}, desc: {desc}, link: {link},avatar:{avatar},cover:{cover} }) CREATE (user)-[r:owner]->(school) RETURN school',
      {
        id: uuid.v4(),
        api_key,
        name:school_data.name,
        desc:school_data.desc,
        link:school_data.link,
        avatar:school_data.avatar,
        cover:school_data.cover
      }
    ).then(async function(results){
        let school = new School(results.records[0].get('school'));
        await setCategories(session,school.id,school_data.categories);
        return school;
      });
};

var setCategories = async function(session,school_id,categories){
  let interest = categories.map(category=>category.id);
  await session.run('Match (school:School {id:{school_id}}) match(category:Category) where category._id in {interest} with school,category create (school)-[:is]->(category)',{school_id,interest});
}

var isSchoolExists = function(session,school_name){
  return session.run('MATCH (school:School {name: {name}}) RETURN school', {name: school_name})
  .then(results => {
    if (!_.isEmpty(results.records)) {
      throw validationRules.SCHOOLNAME_EXISTS;
    }
  });

}

var list = function (session, api_key) {
  return session.run('MATCH (user:User {api_key: {api_key}})-[:owner]->(school:School) RETURN school',
    {
      api_key
    }).then(results => {
      let schools = [] ;
      results.records.forEach(node=>{
        schools.push(new School(node.get('school')));
      });
      return schools;
    });
};


var getById = function (session, school_id) {
  return session.run('MATCH (school:School {id: {school_id} } ) RETURN school',
    {
      school_id
    }).then(results => {
      return new School(results.records[0].get('school'));
    });
};

var listCourses = function (session, school_id) {
  return session.run('MATCH (school:School {id: {school_id} })-[:contain]->(course:Course) RETURN course',
    {
      school_id
    }).then(results => {
      let courses = [] ;
      results.records.forEach(node=>{
        courses.push(new Course(node.get('course')));
      });
      return courses;
    });
};

module.exports = {create,isSchoolExists,list,getById,listCourses};
