"use strict"
var VideoChannelStatus = require('../db/models/videoChannelStatus');
var User = require('../db/models/users');
var videoChannelStatus = function(session,api_key,video_id){
  return session.run('MATCH (user:User {api_key: {api_key}}) MATCH (video:Video {id:{video_id}}) with user,video optional  match (video)<-[:has]-(channel:Channel)<-[subscribe:subscribe]-(user) optional MATCH (user)-[liked:liked]->(video) optional MATCH (user)-[disliked:disliked]->(video) return disliked,liked,subscribe'
  , {api_key,video_id})
  .then(results => {
    return new VideoChannelStatus(results.records[0]);
  });
}

var update = async function(session,api_key,user){
  await updateUserCategories(session,api_key,user.categories);
  return session.run('MATCH (user:User {api_key: {api_key}} ) set user.first_name = {first_name} , user.last_name = {last_name} , user.email = {email} return user'
  , {
    api_key,
    first_name:user.first_name,
    last_name:user.last_name,
    email:user.email
  }).then(results => {
    return new User(results.records[0].get('user'));
  });
}

var updateUserCategories = async function(session,api_key,categories){
  let interest = categories.map(category=>category.id);
  await session.run('Match (user:User {api_key:{api_key}})-[r:interest]->(category:Category) detach delete r',{api_key});
  await session.run('Match (user:User {api_key:{api_key}}) match(category:Category) where category._id in {interest} with user,category create (user)-[:interest]->(category)',{api_key,interest});
}






module.exports = {videoChannelStatus,update};
