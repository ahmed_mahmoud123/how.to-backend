"use strict"
var validationRules = require('../utils/validationRules');
var Category = require('../db/models/category');


var list = function (session) {
  return session.run('MATCH (category:Category) return category').then(results => {
      let categories = [] ;
      results.records.forEach(node=>{
        categories.push(new Category(node.get('category')));
      });
      return categories;
    });
};


module.exports = {list};
