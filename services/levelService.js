"use strict"
var uuid = require('node-uuid');
var Level = require('../db/models/level');
var Video = require('../db/models/video');


var create = function (session, course_id, level_data) {
    return session.run('MATCH (course:Course {id: {course_id}}) CREATE (level:Level {id: {id}, name: {name}, duration: {duration}}) CREATE (course)-[r:has]->(level) RETURN level',
      {
        id: uuid.v4(),
        course_id,
        name:level_data.name,
        duration:level_data.duration,
      }
    ).then(results => {
        return new Level(results.records[0].get('level'));
      });
};

var list = function (session, course_id) {
  return session.run('MATCH (course:Course {id: {course_id}})-[:has]->(level:Level) RETURN level',
    {
      course_id
    }).then(results => {
      let levels = [] ;
      results.records.forEach(node=>{
        levels.push(new Level(node.get('level')));
      });
      return levels;
    });
};

var listVideos = function (session, level_id) {
  return session.run('MATCH (level:Level {id: {level_id} })-[:has]->(video:Video) RETURN video',
    {
      level_id
    }).then(results => {
      let videos = [] ;
      results.records.forEach(node=>{
        videos.push(new Video(node.get('video')));
      });
      return videos;
    });
};

module.exports = {create,list,listVideos};
