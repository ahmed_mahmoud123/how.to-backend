"use strict"
var uuid = require('node-uuid');
var validationRules = require('../utils/validationRules');
var _ = require('lodash');
var Channel = require('../db/models/channels');
var Video = require('../db/models/video');

var create = async function (session, api_key, channel_data) {
    return session.run('MATCH (user:User {api_key: {api_key}}) CREATE (channel:Channel {id: {id}, name: {name}, desc: {desc}, link: {link},avatar:{avatar},cover:{cover} }) CREATE (user)-[r:has]->(channel) RETURN channel',
      {
        id: uuid.v4(),
        api_key,
        name:channel_data.name,
        desc:channel_data.desc,
        link:channel_data.link,
        avatar:channel_data.avatar,
        cover:channel_data.cover
      }
    ).then(async function(results){
        let channel = new Channel(results.records[0].get('channel'));
        await setCategories(session,channel.id,channel_data.categories);
        return channel;
    });
};

var setCategories = async function(session,channel_id,categories){
  let interest = categories.map(category=>category.id);
  await session.run('Match (channel:Channel {id:{channel_id}}) match(category:Category) where category._id in {interest} with channel,category create (channel)-[:is]->(category)',{channel_id,interest});
}

var list = function (session, api_key) {
  return session.run('MATCH (user:User {api_key: {api_key}})-[:has]->(channels:Channel) RETURN channels',
    {
      api_key
    }).then(results => {
      let channels = [] ;
      results.records.forEach(node=>{
        channels.push(new Channel(node.get('channels')));
      });
      return channels;
    });
};

var getById = function (session, channel_id) {
  return session.run('MATCH (channel:Channel {id: {channel_id} } ) RETURN channel',
    {
      channel_id
    }).then(results => {
      return new Channel(results.records[0].get('channel'));
    });
};

var listVideos = function (session, channel_id) {
  return session.run('MATCH (channel:Channel {id: {channel_id} })-[:has]->(video:Video) RETURN video',
    {
      channel_id
    }).then(results => {
      let videos = [] ;
      results.records.forEach(node=>{
        videos.push(new Video(node.get('video')));
      });
      return videos;
    });
};

var isChannelExists = function(session,channel_name){
  return session.run('MATCH (channel:Channel {name: {name}}) RETURN channel', {name: channel_name})
  .then(results => {
    if (!_.isEmpty(results.records)) {
      throw validationRules.CHANNELNAME_EXISTS;
    }
  });
}

var subscribe = async function(session,api_key,channel_id){
  //await unsubscribe(session,api_key,channel_id);
  return session.run('MATCH (user:User {api_key: {api_key}}) MATCH (channel:Channel {id:{channel_id}}) MERGE (user)-[subscribe:subscribe]->(channel) RETURN subscribe', {api_key,channel_id})
  .then(results => {
    return { result:1}
  });
}

var unsubscribe = function(session,api_key,channel_id){
  return session.run('MATCH (user:User {api_key: {api_key}}) MATCH (channel:Channel {id:{channel_id}}) with user,channel match (user)-[subscribe:subscribe]->(channel) detach delete subscribe RETURN subscribe', {api_key,channel_id})
  .then(results => {
    return { result:1}
  });
}




module.exports = {create,isChannelExists,list,getById,listVideos,subscribe,unsubscribe};
