var Channels = require('../services/channelService')
  ,path = require('path')
  , writeResponse = require('../utils/response').writeResponse
  , writeError = require('../utils/response').writeError
  , dbUtils = require('../utils/neo4j/dbUtils')
  , validator = require('../utils/validator')
  , validationRules = require('../utils/validationRules')
  , fileUpload = require('../utils/fileUpload')
  , s3Bucket = require('../utils/s3Bucket')
  , uuid = require('node-uuid');

exports.create = async function (req, res, next) {
  try {
    let result = await fileUpload(req);
    validator(req,{
      "name":[validationRules.REQUIRED],
      "desc":[validationRules.REQUIRED],
      "link":[validationRules.REQUIRED],
      "categories":[validationRules.REQUIRED],
    });
    let dbSession = dbUtils.getSession(req);
    await Channels.isChannelExists(dbSession,result.body.name);
    let channel_data = {categories:JSON.parse(req.body.categories),name:result.body.name,desc:result.body.desc,link:result.body.link,avatar:'default',cover:'default'};
    if(result.files.avatar){
      result.files.avatar.name=path.join(req.user.api_key,uuid.v4()+result.files.avatar.name);
      channel_data.avatar = result.files.avatar.name;
      await s3Bucket(result.files.avatar);
    }
    if(result.files.cover){
      result.files.cover.name=path.join(req.user.api_key,uuid.v4()+result.files.cover.name);
      channel_data.cover = result.files.cover.name;
      await s3Bucket(result.files.cover);
    }
    let channel = await Channels.create(dbSession,req.user.api_key,channel_data);
    writeResponse(res,channel,200);
  } catch (error) {
    writeError(res,error,500);
  }
};

exports.list = async function(req,res,next){
  try{
    let dbSession = dbUtils.getSession(req);
    let channels = await Channels.list(dbSession,req.user.api_key);
    writeResponse(res,channels,200);
  } catch (error) {
    writeError(res,error,500);
  }
}

exports.listVideos = async function(req,res,next){
  try{
    let dbSession = dbUtils.getSession(req);
    if(!req.params.channel_id) throw validationRules.MISSING_CHANNELID;
    let videos = await Channels.listVideos(dbSession,req.params.channel_id);
    writeResponse(res,videos,200);
  } catch (error) {
    writeError(res,error,500);
  }
}


exports.getById = async function(req,res,next){
  try{
    if(!req.params.channel_id) throw validationRules.MISSING_CHANNELID;
    let dbSession = dbUtils.getSession(req);
    let channel = await Channels.getById(dbSession,req.params.channel_id);
    writeResponse(res,channel,200);
  } catch (error) {
    writeError(res,error,500);
  }
}

exports.subscribe = async function(req,res,next){
  try{
    if(!req.params.channel_id) throw validationRules.MISSING_CHANNELID;
    let dbSession = dbUtils.getSession(req);
    let result = await Channels.subscribe(dbSession,req.user.api_key,req.params.channel_id);
    writeResponse(res,result,200);
  } catch (error) {
    writeError(res,error,500);
  }
}

exports.unsubscribe = async function(req,res,next){
  try{
    if(!req.params.channel_id) throw validationRules.MISSING_CHANNELID;
    let dbSession = dbUtils.getSession(req);
    let result = await Channels.unsubscribe(dbSession,req.user.api_key,req.params.channel_id);
    writeResponse(res,result,200);
  } catch (error) {
    writeError(res,error,500);
  }
}
