var Course = require('../services/courseService')
  ,path = require('path')
  , writeResponse = require('../utils/response').writeResponse
  , writeError = require('../utils/response').writeError
  , dbUtils = require('../utils/neo4j/dbUtils')
  , validator = require('../utils/validator')
  , validationRules = require('../utils/validationRules')
  , fileUpload = require('../utils/fileUpload')
  , s3Bucket = require('../utils/s3Bucket')
  , uuid = require('node-uuid');


exports.create = async function (req, res, next) {
  try {
    let result = await fileUpload(req);
    validator(req,{
      "name":[validationRules.REQUIRED],
      "desc":[validationRules.REQUIRED],
      "school_id":[validationRules.REQUIRED]
    });
    let course_data = {name:result.body.name,desc:result.body.desc,avatar:'default',cover:'default'};

    if(result.files.avatar){
      result.files.avatar.name=path.join(req.user.api_key,uuid.v4()+result.files.avatar.name);
      course_data.avatar = result.files.avatar.name;
      await s3Bucket(result.files.avatar);
    }
    if(result.files.cover){
      result.files.cover.name=path.join(req.user.api_key,uuid.v4()+result.files.cover.name);
      course_data.cover = result.files.cover.name;
      await s3Bucket(result.files.cover);
    }
    let dbSession = dbUtils.getSession(req);
    let channel = await Course.create(dbSession,req.body.school_id,course_data);
    writeResponse(res,channel,200);
  } catch (error) {
    writeError(res,error,500);
  }
};

exports.getById = async function(req,res,next){
  try{
    if(!req.params.course_id) throw validationRules.MISSING_COURSEID;
    let dbSession = dbUtils.getSession(req);
    let course = await Course.getById(dbSession,req.params.course_id);
    writeResponse(res,course,200);
  } catch (error) {
    writeError(res,error,500);
  }
}
