var Users = require('../services/userService')
  , writeResponse = require('../utils/response').writeResponse
  , writeError = require('../utils/response').writeError
  , validator = require('../utils/validator')
  , validationRules = require('../utils/validationRules')
  , dbUtils = require('../utils/neo4j/dbUtils');

exports.videoChannelStatus = async function(req,res,next){
  try{
    if(!req.params.video_id) throw validationRules.MISSING_VIDEOID;
    let dbSession = dbUtils.getSession(req);
    let result = await Users.videoChannelStatus(dbSession,req.user.api_key,req.params.video_id);
    writeResponse(res,result,200);
  } catch (error) {
    writeError(res,error,500);
  }
}


exports.update = async function(req,res,next){
  try{
    let body = validator(req,{
      "first_name":[validationRules.REQUIRED],
      "last_name":[validationRules.REQUIRED],
      "email":[validationRules.REQUIRED,validationRules.REQUIRED],
      "categories":[validationRules.REQUIRED]
    });
    let dbSession = dbUtils.getSession(req);
    let result = await Users.update(dbSession,req.user.api_key,body);
    writeResponse(res,result,200);
  } catch (error) {
    console.log(error);
    writeError(res,error,500);
  }
}
