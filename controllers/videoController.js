var Video = require('../services/videoService')
  ,path = require('path')
  , writeResponse = require('../utils/response').writeResponse
  , writeError = require('../utils/response').writeError
  , dbUtils = require('../utils/neo4j/dbUtils')
  , validator = require('../utils/validator')
  , validationRules = require('../utils/validationRules')
  , fileUpload = require('../utils/fileUpload')
  , s3Bucket = require('../utils/s3Bucket')
  , uuid = require('node-uuid');

exports.publish = async function (req, res, next) {
  try {
    let result = await fileUpload(req);
    validator(req,{
      "name":[validationRules.REQUIRED],
      "desc":[validationRules.REQUIRED],
      "duration":[validationRules.REQUIRED],
      'relation_type':[validationRules.REQUIRED],
      'relation_id':[validationRules.REQUIRED]
    });
    let relation_type = _.upperFirst(_.toLower(req.body.relation_type));
    if(relation_type != "Level" && relation_type != "Channel" ) {
      throw validationRules.INVALID_RELATION;
    }
    let video_data = {name:result.body.name,duration:result.body.duration,desc:result.body.desc,cover:'default'};

    if(!result.files.link)
      throw validationRules.INVALID_VIDEO_LINK

    if(result.files.link){
      result.files.link.name=path.join(req.user.api_key,uuid.v4()+result.files.link.name);
      video_data.link = result.files.link.name;
      await s3Bucket(result.files.link);
    }

    if(result.files.cover){
      result.files.cover.name=path.join(req.user.api_key,uuid.v4()+result.files.cover.name);
      video_data.cover = result.files.cover.name;
      await s3Bucket(result.files.cover);
    }
    let dbSession = dbUtils.getSession(req);
    let video = await Video.publish(dbSession,{
      type:relation_type,id:req.body.relation_id
    },video_data);
    writeResponse(res,video,200);
  } catch (error) {
    console.log(error);
    writeError(res,error,500);
  }
};

exports.list = async function(req,res,next){
  try{
    let dbSession = dbUtils.getSession(req);
    let videos = await Video.list(dbSession);
    writeResponse(res,videos,200);
  } catch (error) {
    console.log(error);
    writeError(res,error,500);
  }
}

exports.getById = async function(req,res,next){
  try{
    if(!req.params.video_id) throw validationRules.MISSING_VIDEOID;
    let dbSession = dbUtils.getSession(req);
    let video = await Video.getById(dbSession,req.params.video_id);
    writeResponse(res,video,200);
  } catch (error) {
    writeError(res,error,500);
  }
}

exports.like = async function(req,res,next){
  try{
    if(!req.params.video_id) throw validationRules.MISSING_VIDEOID;
    let dbSession = dbUtils.getSession(req);
    let result = await Video.like(dbSession,req.user.api_key,req.params.video_id);
    writeResponse(res,result,200);
  } catch (error) {
    console.log(error);
    writeError(res,error,500);
  }
}

exports.dislike = async function(req,res,next){
  try{
    if(!req.params.video_id) throw validationRules.MISSING_VIDEOID;
    let dbSession = dbUtils.getSession(req);
    let result = await Video.dislike(dbSession,req.user.api_key,req.params.video_id);
    writeResponse(res,result,200);
  } catch (error) {
    writeError(res,error,500);
  }
}
