var Category = require('../services/categoryService')
  ,path = require('path')
  , writeResponse = require('../utils/response').writeResponse
  , writeError = require('../utils/response').writeError
  , dbUtils = require('../utils/neo4j/dbUtils')




exports.list = async function(req,res,next){
  try{
    let dbSession = dbUtils.getSession(req);
    let categories = await Category.list(dbSession);
    writeResponse(res,categories,200);
  } catch (error) {
    writeError(res,error,500);
  }
}
