var Users = require('../services/authService')
  , writeResponse = require('../utils/response').writeResponse
  , writeError = require('../utils/response').writeError
  , dbUtils = require('../utils/neo4j/dbUtils')
  , validator = require('../utils/validator')
  , validationRules = require('../utils/validationRules');

exports.register = async function (req, res, next) {
  try{
    let body = validator(req,{
      "first_name":[validationRules.REQUIRED],
      "last_name":[validationRules.REQUIRED],
      "email":[validationRules.REQUIRED,validationRules.REQUIRED],
      "password":[validationRules.REQUIRED]
    });
    let response = await Users.register(dbUtils.getSession(req), body);
    writeResponse(res, response, 200);
  }catch(error){
    writeError(res,error,500);
  }
};

exports.login = async function (req, res, next) {
  try{
    let body = validator(req,{
      "email":[validationRules.REQUIRED],
      "password":[validationRules.REQUIRED]
    });
    let response = await Users.login(dbUtils.getSession(req), body.email, body.password);
    writeResponse(res, response);
  }catch(error){
    console.log(error);
    writeError(res,error,400);
  }
};

exports.me = function (req, res, next) {
    writeResponse(res, req.user);
};
