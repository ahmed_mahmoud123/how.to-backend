var Level = require('../services/levelService')
  , writeResponse = require('../utils/response').writeResponse
  , writeError = require('../utils/response').writeError
  , dbUtils = require('../utils/neo4j/dbUtils')
  , validationRules = require('../utils/validationRules')
  , validator = require('../utils/validator');




exports.create = async function(req,res,next){
  try{
    validator(req,{
      "name":[validationRules.REQUIRED],
      "duration":[validationRules.REQUIRED],
      "course_id":[validationRules.REQUIRED]
    });
    let level_data = {name:req.body.name,duration:req.body.duration,course_id:req.body.course_id};
    let dbSession = dbUtils.getSession(req);
    let level = await Level.create(dbSession,req.body.course_id,level_data);
    writeResponse(res,level,200);
  } catch (error) {
    writeError(res,error,500);
  }
}

exports.list = async function(req,res,next){
  try{
    if(!req.params.course_id) throw validationRules.MISSING_COURSEID;
    let dbSession = dbUtils.getSession(req);
    let levels = await Level.list(dbSession,req.params.course_id);
    writeResponse(res,levels,200);
  } catch (error) {
    writeError(res,error,500);
  }
}

exports.listVideos = async function(req,res,next){
  try{
    let dbSession = dbUtils.getSession(req);
    if(!req.params.level_id) throw validationRules.MISSING_LEVELID;
    let videos = await Level.listVideos(dbSession,req.params.level_id);
    writeResponse(res,videos,200);
  } catch (error) {
    writeError(res,error,500);
  }
}
