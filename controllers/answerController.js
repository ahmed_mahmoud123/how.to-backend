var Answer = require('../services/answerService')
  ,path = require('path')
  , writeResponse = require('../utils/response').writeResponse
  , writeError = require('../utils/response').writeError
  , dbUtils = require('../utils/neo4j/dbUtils')
  , validator = require('../utils/validator')
  , validationRules = require('../utils/validationRules');

exports.create = async function (req, res, next) {
  try {
    validator(req,{
      "answer":[validationRules.REQUIRED],
      "video_id":[validationRules.REQUIRED]
    });
    let dbSession = dbUtils.getSession(req);
    let answer = await Answer.create(dbSession,req.user.api_key,{
      answer:req.body.answer,
      video_id:req.body.video_id
    });
    writeResponse(res,answer,200);
  } catch (error) {
    console.log(error);
    writeError(res,error,500);
  }
};


exports.list = async function(req,res,next){
  try{
    if(!req.params.video_id) throw validationRules.MISSING_VIDEOID;
    let dbSession = dbUtils.getSession(req);
    let answers = await Answer.list(dbSession,req.params.video_id);
    writeResponse(res,answers,200);
  } catch (error) {
    console.log(error);
    writeError(res,error,500);
  }
}
