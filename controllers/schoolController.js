var School = require('../services/schoolService')
  ,path = require('path')
  , writeResponse = require('../utils/response').writeResponse
  , writeError = require('../utils/response').writeError
  , dbUtils = require('../utils/neo4j/dbUtils')
  , validator = require('../utils/validator')
  , validationRules = require('../utils/validationRules')
  , fileUpload = require('../utils/fileUpload')
  , s3Bucket = require('../utils/s3Bucket')
  , uuid = require('node-uuid');

exports.create = async function (req, res, next) {
  try {
    let result = await fileUpload(req);
    validator(req,{
      "name":[validationRules.REQUIRED],
      "desc":[validationRules.REQUIRED],
      "link":[validationRules.REQUIRED],
      "categories":[validationRules.REQUIRED],
    });
    let dbSession = dbUtils.getSession(req);
    await School.isSchoolExists(dbSession,result.body.name);
    let school_data = {categories:JSON.parse(result.body.categories),name:result.body.name,desc:result.body.desc,link:result.body.link,avatar:'default',cover:'default'};
    if(result.files.avatar){
      result.files.avatar.name=path.join(req.user.api_key,uuid.v4()+result.files.avatar.name);
      school_data.avatar = result.files.avatar.name;
      await s3Bucket(result.files.avatar);
    }
    if(result.files.cover){
      result.files.cover.name=path.join(req.user.api_key,uuid.v4()+result.files.cover.name);
      school_data.cover = result.files.cover.name;
      await s3Bucket(result.files.cover);
    }
    let channel = await School.create(dbSession,req.user.api_key,school_data);
    writeResponse(res,channel,200);
  } catch (error) {
    writeError(res,error,500);
  }
};


exports.list = async function(req,res,next){
  try{
    let dbSession = dbUtils.getSession(req);
    let schools = await School.list(dbSession,req.user.api_key);
    writeResponse(res,schools,200);
  } catch (error) {
    writeError(res,error,500);
  }
}

exports.getById = async function(req,res,next){
  try{
    if(!req.params.school_id) throw validationRules.MISSING_SCHOOLID;
    let dbSession = dbUtils.getSession(req);
    let school = await School.getById(dbSession,req.params.school_id);
    writeResponse(res,school,200);
  } catch (error) {
    writeError(res,error,500);
  }
}


exports.listCourses = async function(req,res,next){
  try{
    let dbSession = dbUtils.getSession(req);
    if(!req.params.school_id) throw validationRules.MISSING_SCHOOLID;
    let courses = await School.listCourses(dbSession,req.params.school_id);
    writeResponse(res,courses,200);
  } catch (error) {
    writeError(res,error,500);
  }
}
