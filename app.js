var express = require('express') ,
path = require('path'),
app = express(),
config = require('./config')(process.env.NODE_ENV || 'dev');

require('./routes')(app);


app.listen(config.PORT);
