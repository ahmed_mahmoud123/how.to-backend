const seed = require('neo4j-seed');
// neo4j cypher helper module
var config = require('./config')(process.env.NODE_ENV || 'dev');
var neo4j = require('neo4j-driver').v1;
var driver = neo4j.driver(config['neo4j-local'], neo4j.auth.basic(config['USERNAME'], config['PASSWORD']));


seed('./db/seeds', driver)
