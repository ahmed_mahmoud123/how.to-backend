var express = require('express');
var router = express.Router();
const busboy = require('connect-busboy');
const busboyBodyParser = require('busboy-body-parser');

var videoController = require('../controllers/videoController');
/**
 * @swagger
 * definition:
 *   Video:
 *     type: object
 *     properties:
 *       id:
 *         type: string
 *       name:
 *         type: string
 *       desc:
 *         type: string
 *       link:
 *         type: string
 *       duration:
 *         type: string
 *       cover:
 *         type: string
 */

/**
 * @swagger
 * /video/publish:
 *   post:
 *     tags:
 *     - Video
 *     description: Publish New Video
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         type: string
 *         required: true
 *         description: Token (token goes here)
 *       - name: body
 *         in: body
 *         type: object
 *         schema:
 *           properties:
 *             name:
 *               type: string
 *             link:
 *               type: string
 *             desc:
 *               type: string
 *             duration:
 *               type: string
 *             relation_type:
 *               type: string
 *               description: must be 'channel' or 'levels'
 *             relation_id:
 *               type: string
 *               description: relation node id
 *       - name: files
 *         description: files.cover & files.link
 *         in: files
 *         type: object
 *         schema:
 *           properties:
 *             link:
 *               type: file
 *             cover:
 *               type: file
 *     responses:
 *       200:
 *         description: publish new Video
 *         schema:
 *           $ref: '#/definitions/Video'
 *       500:
 *         description: Error message(s)
 *       403:
 *         description: invalid / missing authentication
 */
router.post("/publish",[busboy(),busboyBodyParser()],videoController.publish);

/**
 * @swagger
 * /video/list:
 *   get:
 *     tags:
 *     - Video
 *     description: list videos
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         type: string
 *         required: true
 *         description: Token (token goes here)
 *     responses:
 *       200:
 *         description: list videos
 *         schema:
 *           $ref: '#/definitions/Video/list'
 *       500:
 *         description: Error message(s)
 *       403:
 *         description: invalid / missing authentication
 */
router.get("/list",videoController.list);

/**
 * @swagger
 * /video/{video_id}/like :
 *   get:
 *     tags:
 *     - Video
 *     description: like certain video
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         type: string
 *         required: true
 *         description: Token (token goes here)
 *       - name: video_id
 *         in: path
 *         type: string
 *         required: true
 *         description: video id
 *     responses:
 *       200:
 *         description: like certain video
 *         schema:
 *           $ref: '#/definitions/Video/like'
 *       500:
 *         description: Error message(s)
 *       403:
 *         description: invalid / missing authentication
 */
router.get("/:video_id/like",videoController.like);

/**
 * @swagger
 * /video/{video_id}/dislike :
 *   get:
 *     tags:
 *     - Video
 *     description: dislike certain video
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         type: string
 *         required: true
 *         description: Token (token goes here)
 *       - name: video_id
 *         in: path
 *         type: string
 *         required: true
 *         description: video id
 *     responses:
 *       200:
 *         description: like certain video
 *         schema:
 *           $ref: '#/definitions/Video/dislike'
 *       500:
 *         description: Error message(s)
 *       403:
 *         description: invalid / missing authentication
 */
router.get("/:video_id/dislike",videoController.dislike);


/**
 * @swagger
 * /video/{video_id} :
 *   get:
 *     tags:
 *     - Video
 *     description: Get Video Details
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         type: string
 *         required: true
 *         description: Token (token goes here)
 *       - name: video_id
 *         in: path
 *         type: string
 *         required: true
 *         description: video id
 *     responses:
 *       200:
 *         description: Get Video Details
 *         schema:
 *           $ref: '#/definitions/Video/details'
 *       500:
 *         description: Error message(s)
 *       403:
 *         description: invalid / missing authentication
 */
router.get("/:video_id",videoController.getById);


module.exports = router;
