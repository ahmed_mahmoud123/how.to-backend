var express = require('express');
var router = express.Router();
const busboy = require('connect-busboy');
const busboyBodyParser = require('busboy-body-parser');

var channelController = require('../controllers/channelController');
/**
 * @swagger
 * definition:
 *   Channel:
 *     type: object
 *     properties:
 *       id:
 *         type: string
 *       name:
 *         type: string
 *       desc:
 *         type: string
 *       link:
 *         type: string
 *       avatar:
 *         type: string
 *       cover:
 *         type: string
 */

/**
 * @swagger
 * /channel/create:
 *   post:
 *     tags:
 *     - Channel
 *     description: Create New Channel
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         type: string
 *         required: true
 *         description: Token (token goes here)
 *       - name: body
 *         in: body
 *         type: object
 *         schema:
 *           properties:
 *             name:
 *               type: string
 *             link:
 *               type: string
 *             desc:
 *               type: string
 *       - name: files
 *         description: files.avatar & files.cover
 *         in: files
 *         type: object
 *         schema:
 *           properties:
 *             avatar:
 *               type: file
 *             cover:
 *               type: file
 *     responses:
 *       200:
 *         description: Your new Channel
 *         schema:
 *           $ref: '#/definitions/Channel'
 *       500:
 *         description: Error message(s)
 *       403:
 *         description: invalid / missing authentication
 */
router.post("/create",[busboy(),busboyBodyParser()],channelController.create);


/**
 * @swagger
 * /channel/list:
 *   get:
 *     tags:
 *     - Channel
 *     description: List user's channels
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         type: string
 *         required: true
 *         description: Token (token goes here)
 *     responses:
 *       200:
 *         description: List user's channels
 *         schema:
 *           $ref: '#/definitions/Channel/List'
 *       500:
 *         description: Error message(s)
 *       403:
 *         description: invalid / missing authentication
 */
router.get("/list",channelController.list);

/**
 * @swagger
 * /channel/{channel_id}/details:
 *   get:
 *     tags:
 *     - Channel
 *     description: Get Channel Details
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         type: string
 *         required: true
 *         description: Token (token goes here)
 *       - name: channel_id
 *         in: path
 *         type: string
 *         required: true
 *         description: channel id
 *     responses:
 *       200:
 *         description: Channel Details
 *         schema:
 *           $ref: '#/definitions/Channel/Details'
 *       500:
 *         description: Error message(s)
 *       403:
 *         description: invalid / missing authentication
 */
router.get("/:channel_id/details",channelController.getById);

/**
 * @swagger
 * /channel/{channel_id}/subscribe:
 *   get:
 *     tags:
 *     - Channel
 *     description: subscribe in channel
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         type: string
 *         required: true
 *         description: Token (token goes here)
 *       - name: channel_id
 *         in: path
 *         type: string
 *         required: true
 *         description: channel id
 *     responses:
 *       200:
 *         description: subscribe in channel
 *         schema:
 *           $ref: '#/definitions/Channel/subscribe'
 *       500:
 *         description: Error message(s)
 *       403:
 *         description: invalid / missing authentication
 */
router.get("/:channel_id/subscribe",channelController.subscribe);

/**
 * @swagger
 * /channel/{channel_id}/unsubscribe:
 *   get:
 *     tags:
 *     - Channel
 *     description: unsubscribe in channel
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         type: string
 *         required: true
 *         description: Token (token goes here)
 *       - name: channel_id
 *         in: path
 *         type: string
 *         required: true
 *         description: channel id
 *     responses:
 *       200:
 *         description: unsubscribe in channel
 *         schema:
 *           $ref: '#/definitions/Channel/unsubscribe'
 *       500:
 *         description: Error message(s)
 *       403:
 *         description: invalid / missing authentication
 */
router.get("/:channel_id/unsubscribe",channelController.unsubscribe);

/**
 * @swagger
 * /channel/{channel_id}/videos:
 *   get:
 *     tags:
 *     - Channel
 *     description: list channel's videos
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         type: string
 *         required: true
 *         description: Token (token goes here)
 *       - name: channel_id
 *         in: path
 *         type: string
 *         required: true
 *         description: channel id
 *     responses:
 *       200:
 *         description: list channel's videos
 *         schema:
 *           $ref: '#/definitions/Channel/list_videos'
 *       500:
 *         description: Error message(s)
 *       403:
 *         description: invalid / missing authentication
 */
router.get("/:channel_id/videos",channelController.listVideos);

module.exports = router;
