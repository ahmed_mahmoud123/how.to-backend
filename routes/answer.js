var express = require('express');
var router = express.Router();

var answerController = require('../controllers/answerController');
/**
 * @swagger
 * definition:
 *   Answer:
 *     type: object
 *     properties:
 *       id:
 *         type: string
 *       answer:
 *         type: string
 *       video_id:
 *         type: string
 */

/**
 * @swagger
 * /answer/create:
 *   post:
 *     tags:
 *     - Answer
 *     description: Post New Answer
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         type: string
 *         required: true
 *         description: Token (token goes here)
 *       - name: body
 *         in: body
 *         type: object
 *         schema:
 *           properties:
 *             answer:
 *               type: string
 *             video_id:
 *               type: string
 *     responses:
 *       200:
 *         description: Post New Answer
 *         schema:
 *           $ref: '#/definitions/Answer'
 *       500:
 *         description: Error message(s)
 *       403:
 *         description: invalid / missing authentication
 */
router.post("/create",answerController.create);


/**
 * @swagger
 * /answer/{video_id}:
 *   get:
 *     tags:
 *     - Answer
 *     description: Get Video's answers
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         type: string
 *         required: true
 *         description: Token (token goes here)
 *       - name: video_id
 *         in: path
 *         type: string
 *         required: true
 *         description: video id
 *     responses:
 *       200:
 *         description: Get Video's answers
 *         schema:
 *           $ref: '#/definitions/Answer'
 *       500:
 *         description: Error message(s)
 *       403:
 *         description: invalid / missing authentication
 */
router.get("/:video_id",answerController.list);

module.exports = router;
