var express = require('express');
var router = express.Router();

var categoryController = require('../controllers/categoryController');
/**
 * @swagger
 * definition:
 *   Category:
 *     type: object
 *     properties:
 *       id:
 *         type: string
 *       name:
 *         type: string
 */

/**
 * @swagger
 * /category:
 *   get:
 *     tags:
 *     - Category
 *     description: list categories
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         type: string
 *         required: true
 *         description: Token (token goes here)
 *     responses:
 *       200:
 *         description: list categories
 *         schema:
 *           $ref: '#/definitions/Category'
 *       500:
 *         description: Error message(s)
 *       403:
 *         description: invalid / missing authentication
 */


router.get("/",categoryController.list);

module.exports = router;
