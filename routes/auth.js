var express = require('express'),
router = express.Router(),
loginRequired = require('../middlewares/loginRequired'),
authController = require('../controllers/authController');
/**
 * @swagger
 * definition:
 *   User:
 *     type: object
 *     properties:
 *       id:
 *         type: string
 *       first_name:
 *         type: string
 *       last_name:
 *         type: string
 *       email:
 *         type: string
 */

/**
 * @swagger
 * /auth/register:
 *   post:
 *     tags:
 *     - Auth
 *     description: Register a new user
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         type: object
 *         schema:
 *           properties:
 *             first_name:
 *               type: string
 *             last_name:
 *               type: string
 *             email:
 *               type: string
 *             password:
 *               type: string
 *     responses:
 *       200:
 *         description: Your new user
 *         schema:
 *           $ref: '#/definitions/User'
 *       500:
 *         description: Error message(s)
 */
router.post("/register",authController.register);
/**
 * @swagger
 * /auth/login:
 *   post:
 *     tags:
 *     - Auth
 *     description: Login
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         type: object
 *         schema:
 *           properties:
 *             username:
 *               type: string
 *             password:
 *               type: string
 *     responses:
 *       200:
 *         description: succesful login
 *         schema:
 *           properties:
 *             token:
 *               type: string
 *       400:
 *         description: invalid credentials
 */
router.post("/login",authController.login);

/**
 * @swagger
 * /auth/me:
 *   get:
 *     tags:
 *     - Auth
 *     description: Get your user
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         type: string
 *         required: true
 *         description: Token (token goes here)
 *     responses:
 *       200:
 *         description: the user
 *         schema:
 *           $ref: '#/definitions/User'
 *       401:
 *         description: invalid / missing authentication
 */
router.post("/me",loginRequired,authController.me);

module.exports = router;
