var authRouter = require('./auth'),
channelRouter = require('./channel'),
courseRouter = require('./course'),
levelRouter = require('./level'),
userRouter = require('./user'),
categoryRouter = require('./category'),

videoRouter = require('./video'),
answerRouter = require('./answer'),

schoolRouter = require('./school'),
setAuthUser = require('../middlewares/setAuthUser'),
loginRequired = require('../middlewares/loginRequired'),
neo4jSessionCleanup = require('../middlewares/neo4jSessionCleanup'),
bodyParser = require('body-parser')

module.exports = (app)=>{
  //enable CORS
    app.use(function(req, res, next) {
      res.header("Access-Control-Allow-Origin", "*");
      res.header("Access-Control-Allow-Credentials", "true");
      res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT,DELETE");
      res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
      next();
    });

    app.use(bodyParser.json({limit: '10mb', extended: true}));
    app.use(bodyParser.urlencoded({limit: '10mb', extended: true,parameterLimit: 1000000}));
    app.use(setAuthUser);
    app.use(neo4jSessionCleanup);
    require('./swagger')(app);
    app.use("/auth",authRouter);
    app.use("/channel",loginRequired,channelRouter);
    app.use("/school",loginRequired,schoolRouter);
    app.use("/video",loginRequired,videoRouter);
    app.use("/course",loginRequired,courseRouter);
    app.use("/level",loginRequired,levelRouter);
    app.use("/user",loginRequired,userRouter);
    app.use("/answer",loginRequired,answerRouter);
    app.use("/category",loginRequired,categoryRouter);


}
