var express = require('express');
var router = express.Router();

var levelController = require('../controllers/levelController');
/**
 * @swagger
 * definition:
 *   Level:
 *     type: object
 *     properties:
 *       id:
 *         type: string
 *       name:
 *         type: string
 *       duration:
 *         type: string
 */

/**
 * @swagger
 * /level/create:
 *   post:
 *     tags:
 *     - CourseLevel
 *     description: Create New CourseLevel
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         type: string
 *         required: true
 *         description: Token (token goes here)
 *       - name: body
 *         in: body
 *         type: object
 *         schema:
 *           properties:
 *             name:
 *               type: string
 *             course_id:
 *               type: string
 *             duration:
 *               type: string
 *     responses:
 *       200:
 *         description: Your new CourseLevel
 *         schema:
 *           $ref: '#/definitions/CourseLevel'
 *       500:
 *         description: Error message(s)
 *       403:
 *         description: invalid / missing authentication
 */
router.post("/create",levelController.create);

/**
 * @swagger
 * /level/{course_id}:
 *   get:
 *     tags:
 *     - CourseLevel
 *     description: list course's levels
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         type: string
 *         required: true
 *         description: Token (token goes here)
 *       - name: course_id
 *         in: path
 *         type: string
 *         required: true
 *         description: course id
 *     responses:
 *       200:
 *         description: list course's levels
 *         schema:
 *           $ref: '#/definitions/level/list'
 *       500:
 *         description: Error message(s)
 *       403:
 *         description: invalid / missing authentication
 */
router.get("/:course_id",levelController.list);


/**
 * @swagger
 * /level/{level_id}/videos:
 *   get:
 *     tags:
 *     - CourseLevel
 *     description: list level's videos
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         type: string
 *         required: true
 *         description: Token (token goes here)
 *       - name: level_id
 *         in: path
 *         type: string
 *         required: true
 *         description: level id
 *     responses:
 *       200:
 *         description: list level's videos
 *         schema:
 *           $ref: '#/definitions/level/videos'
 *       500:
 *         description: Error message(s)
 *       403:
 *         description: invalid / missing authentication
 */
router.get("/:level_id/videos",levelController.listVideos);

module.exports = router;
