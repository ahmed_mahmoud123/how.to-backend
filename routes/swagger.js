var swaggerUi = require('swagger-ui-express'),
swaggerJSDoc = require('swagger-jsdoc'),
swaggerDefinition = {
  info: {
    title: 'How.to APIs',
    version: '1.0.0',
    description: '',
  },
  host: 'https://api.how.to',
  basePath: '/',
},

// options for the swagger docs
options = {
  // import swaggerDefinitions
  swaggerDefinition: swaggerDefinition,
  // path to the API docs
  apis: ['./routes/*.js'],
},
swaggerSpec = swaggerJSDoc(options);

module.exports = (app)=>{
  app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));
}
