var express = require('express');
var router = express.Router();
const busboy = require('connect-busboy');
const busboyBodyParser = require('busboy-body-parser');

var courseController = require('../controllers/courseController');
/**
 * @swagger
 * definition:
 *   Course:
 *     type: object
 *     properties:
 *       id:
 *         type: string
 *       name:
 *         type: string
 *       desc:
 *         type: string
 *       avatar:
 *         type: string
 *       cover:
 *         type: string
 */

/**
 * @swagger
 * /course/create:
 *   post:
 *     tags:
 *     - Course
 *     description: Create New Course
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         type: string
 *         required: true
 *         description: Token (token goes here)
 *       - name: body
 *         in: body
 *         type: object
 *         schema:
 *           properties:
 *             name:
 *               type: string
 *             school_id:
 *               type: string
 *             desc:
 *               type: string
 *       - name: files
 *         description: files.avatar & files.cover
 *         in: files
 *         type: object
 *         schema:
 *           properties:
 *             avatar:
 *               type: file
 *             cover:
 *               type: file
 *     responses:
 *       200:
 *         description: Your new Course
 *         schema:
 *           $ref: '#/definitions/Course'
 *       500:
 *         description: Error message(s)
 *       403:
 *         description: invalid / missing authentication
 */
router.post("/create",[busboy(),busboyBodyParser()],courseController.create);




/**
 * @swagger
 * /course/{course_id}/details:
 *   get:
 *     tags:
 *     - Course
 *     description: Get Course Details
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         type: string
 *         required: true
 *         description: Token (token goes here)
 *       - name: course_id
 *         in: path
 *         type: string
 *         required: true
 *         description: course id
 *     responses:
 *       200:
 *         description: Course Details
 *         schema:
 *           $ref: '#/definitions/Course/Details'
 *       500:
 *         description: Error message(s)
 *       403:
 *         description: invalid / missing authentication
 */
router.get("/:course_id/details",courseController.getById);

module.exports = router;
