var express = require('express');
var router = express.Router();
const busboy = require('connect-busboy');
const busboyBodyParser = require('busboy-body-parser');

var schoolController = require('../controllers/schoolController');
/**
 * @swagger
 * definition:
 *   School:
 *     type: object
 *     properties:
 *       id:
 *         type: string
 *       name:
 *         type: string
 *       desc:
 *         type: string
 *       link:
 *         type: string
 *       avatar:
 *         type: string
 *       cover:
 *         type: string
 */

/**
 * @swagger
 * /school/create:
 *   post:
 *     tags:
 *     - School
 *     description: Create New School
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         type: string
 *         required: true
 *         description: Token (token goes here)
 *       - name: body
 *         in: body
 *         type: object
 *         schema:
 *           properties:
 *             name:
 *               type: string
 *             link:
 *               type: string
 *             desc:
 *               type: string
 *       - name: files
 *         description: files.avatar & files.cover
 *         in: files
 *         type: object
 *         schema:
 *           properties:
 *             avatar:
 *               type: file
 *             cover:
 *               type: file
 *     responses:
 *       200:
 *         description: Your new School
 *         schema:
 *           $ref: '#/definitions/School'
 *       500:
 *         description: Error message(s)
 *       403:
 *         description: invalid / missing authentication
 */
router.post("/create",[busboy(),busboyBodyParser()],schoolController.create);

/**
 * @swagger
 * /school/list:
 *   get:
 *     tags:
 *     - School
 *     description: List user's schools
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         type: string
 *         required: true
 *         description: Token (token goes here)
 *     responses:
 *       200:
 *         description: List user's schools
 *         schema:
 *           $ref: '#/definitions/School/List'
 *       500:
 *         description: Error message(s)
 *       403:
 *         description: invalid / missing authentication
 */
router.get("/list",schoolController.list);

/**
 * @swagger
 * /school/{school_id}/details:
 *   get:
 *     tags:
 *     - School
 *     description: Get School Details
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         type: string
 *         required: true
 *         description: Token (token goes here)
 *       - name: school_id
 *         in: path
 *         type: string
 *         required: true
 *         description: school id
 *     responses:
 *       200:
 *         description: Channel Details
 *         schema:
 *           $ref: '#/definitions/School/Details'
 *       500:
 *         description: Error message(s)
 *       403:
 *         description: invalid / missing authentication
 */
router.get("/:school_id/details",schoolController.getById);


/**
 * @swagger
 * /school/{school_id}/courses:
 *   get:
 *     tags:
 *     - School
 *     description: list school's courses
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         type: string
 *         required: true
 *         description: Token (token goes here)
 *       - name: school_id
 *         in: path
 *         type: string
 *         required: true
 *         description: school id
 *     responses:
 *       200:
 *         description: list school's courses
 *         schema:
 *           $ref: '#/definitions/School/list_courses'
 *       500:
 *         description: Error message(s)
 *       403:
 *         description: invalid / missing authentication
 */
router.get("/:school_id/courses",schoolController.listCourses);

module.exports = router;
