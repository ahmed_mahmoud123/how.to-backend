var express = require('express');
var router = express.Router();
const busboy = require('connect-busboy');
const busboyBodyParser = require('busboy-body-parser');

var userController = require('../controllers/userController');
/**
 * @swagger
 * definition:
 *   User:
 *     type: object
 *     properties:
 *       id:
 *         type: string
 *       first_name:
 *         type: string
 *       last_name:
 *         type: string
 *       email:
 *         type: string
 */

 

router.get("/videoChannelStatus/:video_id",userController.videoChannelStatus);
router.post("/update",userController.update);

module.exports = router;
