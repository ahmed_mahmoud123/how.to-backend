var _ = require('lodash');

var Course = module.exports = function (_node) {
  var name = _node.properties['name'];
  var desc = _node.properties['desc'];
  var avatar = _node.properties['avatar'];
  var cover = _node.properties['cover'];
  var id = _node.properties['id'];
  _.extend(this, {
    id,
    name,
    desc,
    avatar,
    cover
  });
};
