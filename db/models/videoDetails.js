var _ = require('lodash');
var Channel = require('./channels');
var Video = require('./video');
var Level = require('./level');
var Course = require('./course');

var VideoDetails = module.exports = function (_node) {
  let videoDetails = Object.create({});
  if(_node.get('video'))
    videoDetails.video = new Video(_node.get('video'));
  if(_node.get('channel'))
    videoDetails.channel = new Channel(_node.get('channel'));
  if(_node.get('level'))
    videoDetails.level = new Level(_node.get('level'));
  if(_node.get('course'))
    videoDetails.course = new Course(_node.get('course'));

  _.extend(this,videoDetails);
};
