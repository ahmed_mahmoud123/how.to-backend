var _ = require('lodash');

var VideoChannelStatus = module.exports = function (_node) {
  let output = {liked:false,disliked:false,subscribe:false};
  if(_node.get('liked')){
    output.liked = true;
  }
  if(_node.get('disliked')){
    output.disliked = true;
  }
  if(_node.get('subscribe')){
    output.subscribe = true;
  }


  _.extend(this,output);
};
