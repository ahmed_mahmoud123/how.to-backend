var _ = require('lodash');
var Category = require('./category');
var User = module.exports = function (_node,_category=undefined) {
  var first_name = _node.properties['first_name'];
  let categories = [];
  if(_category){
  _category.records.forEach(cat=>{
    if(cat.get('category'))
      categories.push(new Category(cat.get('category')));
  });
  }
  _.extend(this, {
    'id': _node.properties['id'],
    'first_name': first_name,
    'last_name': _node.properties['last_name'],
    'email': _node.properties['email'],
    'api_key': _node.properties['api_key'],
    categories
  });
};
