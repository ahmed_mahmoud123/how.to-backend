var _ = require('lodash');

var Level = module.exports = function (_node) {
  var name = _node.properties['name'];
  var id = _node.properties['_id'];
  _.extend(this, {
    id,
    name
  });
};
