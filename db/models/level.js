var _ = require('lodash');

var Level = module.exports = function (_node) {
  var name = _node.properties['name'];
  var duration = _node.properties['duration'];
  var id = _node.properties['id'];
  _.extend(this, {
    id,
    name,
    duration
  });
};
