var _ = require('lodash');
var User = require('./users');
var Answer = module.exports = function (_node) {
  let output = {
    user:new User(_node.get('user')),
    answer:{
      text:_node.get('answer').properties['text'],
      createdAt : _node.get('answer').properties['createdAt']
    }
  };
  _.extend(this,output);
};
