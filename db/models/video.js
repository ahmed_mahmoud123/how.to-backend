var _ = require('lodash');

var Video = module.exports = function (_node) {
  var name = _node.properties['name'];
  var desc = _node.properties['desc'];
  var duration = _node.properties['duration'];
  var cover = _node.properties['cover'];
  var id = _node.properties['id'];
  var link = _node.properties['link'];
  _.extend(this, {
    id,
    name,
    desc,
    duration,
    cover,
    link
  });
};
