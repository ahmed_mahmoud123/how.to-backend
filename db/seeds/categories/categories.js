MERGE (:Category {_id:"1",name: 'Narrative'})
MERGE (:Category {_id:"2",name: 'Travel'})
MERGE (:Category {_id:"3",name: 'Animation'})
MERGE (:Category {_id:"4",name: 'Comedy'})
MERGE (:Category {_id:"5",name: 'Sports'})
MERGE (:Category {_id:"6",name: 'Music'})
MERGE (:Category {_id:"7",name: 'Talks'})
MERGE (:Category {_id:"8",name: 'Arts & Design'})
MERGE (:Category {_id:"9",name: 'Fashion'})
MERGE (:Category {_id:"10",name: 'Instructionals'})
MERGE (:Category {_id:"11",name: 'Personal'})
MERGE (:Category {_id:"12",name: 'Reporting & Journalism'})
MERGE (:Category {_id:"13",name: 'Cameras & Techniques'})
MERGE (:Category {_id:"14",name: 'Food'})
MERGE (:Category {_id:"15",name: 'Experimental'})
MERGE (:Category {_id:"16",name: 'Documentary'})
