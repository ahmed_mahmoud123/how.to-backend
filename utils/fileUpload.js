const Busboy = require('busboy');

module.exports = (req)=>{
  return new Promise(resolve=>{
      var busboy = new Busboy({ headers: req.headers });
      busboy.on('finish', function() {
        resolve({body:req.body,files:req.files});
      });
      req.pipe(busboy);
  });

}
