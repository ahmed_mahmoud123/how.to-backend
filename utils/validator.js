_ = require('lodash'),
validator = require('validator');

validator.isNotEmpty = function(value) {
  return value?true:false;
};

module.exports = (req,validationObject)=>{
  let errors = [];
  let body = {};
  for (var field in validationObject) {
    validationObject[field].forEach(rule=>{
      rule = Object.assign({},rule);
      body[field] =  _.get(req.body, field);
      if(!validator[rule.rule](body[field])){
        rule.msg = rule.msg.replace(/%s/,field);
        errors.push(rule);
      }
    });
  }
  if(errors != 0)
    throw errors
  return body;
}
