const AWS = require('aws-sdk');
var config = require('../config')(process.env.NODE_ENV || 'dev');
let s3bucket = new AWS.S3({
   accessKeyId: config.IAM_USER_KEY,
   secretAccessKey: config.IAM_USER_SECRET,
   Bucket: config.IMAGES_BUCKET_NAME,
 });

module.exports = (file)=>{
  return new Promise(resolve=>{
    s3bucket.createBucket(function () {
      var params = {
       Bucket: config.IMAGES_BUCKET_NAME,
       Key: file.name,
       Body: file.data,
      };
      s3bucket.upload(params, function (err, data) {
       if (err) {
         throw {err};
       }
       resolve(data);
      });
    });
  })
};
