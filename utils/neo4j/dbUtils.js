"use strict";

// neo4j cypher helper module
var config = require('../../config')(process.env.NODE_ENV || 'dev');

var neo4j = require('neo4j-driver').v1;
var driver = neo4j.driver(config['neo4j-local'], neo4j.auth.basic(config['USERNAME'], config['PASSWORD']));

exports.getSession = function (context) {
  if(context.neo4jSession) {
    return context.neo4jSession;
  }
  else {
    context.neo4jSession = driver.session();
    return context.neo4jSession;
  }
};
