module.exports = {
  "MISSING_LEVELID":{
    msg:"level id is missing",
  },
  "MISSING_COURSEID":{
    msg:"course id is missing",
  },
  "MISSING_SCHOOLID":{
    msg:"school id is missing",
  },
  "MISSING_VIDEOID":{
    msg:"video id is missing",
  },
  "MISSING_CHANNELID":{
    msg:"channel id is missing",
  },
  "WRONG_PASSWORD":{
    msg:"password is wrong",
  },
  "INVALID_USER":{
    msg:"email is not exists",
  },
  "EMAIL_EXISTS":{
    msg:"email already in use",
  },
  "CHANNELNAME_EXISTS":{
    msg:"channel name already in use",
  },
  "SCHOOLNAME_EXISTS":{
    msg:"school name already in use",
  },
  "INVALID_VIDEO_LINK":{
    msg:"invalid video file ",
  },
  "REQUIRED":{
    rule:"isNotEmpty",
    msg:"%s is required",
  },
  "EMAIL":{
    rule:"isEmail",
    msg:"%s is invalid email",
  },
  "INVALID_RELATION":{
    msg:"invalid relation",
  },
  "INVALID_TOKEN":{
    msg:'invalid authorization format. Follow `Token <token>`'
  },
  "INVALID_AUTHORIZATION":{
    msg:'no authorization provided'
  },
  "INVALID_APIKEY":{
    msg:'invalid api key provided'
  }
}
