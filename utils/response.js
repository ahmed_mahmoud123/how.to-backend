var sw = require("swagger-node-express");
var _ = require('lodash');

exports.writeResponse = function writeResponse(res, response, status) {
  sw.setHeaders(res);
  res.status(status || 200).json(response);
};

exports.writeError = function writeError(res, error, status) {
  sw.setHeaders(res);
  if(!Array.isArray(error))
    error = [error];
  res.status(error.status || status || 400).json(error);
};
